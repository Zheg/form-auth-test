package me.freedom4live.ktor

object Cookies {
    const val AUTH_COOKIE = "auth"
}

object AuthName {
    const val SESSION = "auth_session"
    const val FORM = "auth_form"
}

object FormFields {
    const val USERNAME = "auth_username_field"
    const val PASSWORD = "auth_password_field"
    const val INPUT_BUTTON = "auth_input_button"
}